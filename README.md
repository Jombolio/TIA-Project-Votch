# The Infinite Azure - Project Votch
## Open Source Project forked from OSP!

This project aims to help content creators have a safe space and not have to worry about the capitalist corporate machinations that is Copyright management on YouTube or Twitch.

[![N|Solid](https://i.imgur.com/LSaHWfi.png)](https://i.imgur.com/jYjVTPT.jpg)

## Features
- Discord Integration (soon)
- Ad Free

# Open Streaming Platform

[![N|Solid](https://i.imgur.com/WyrOl6y.png)](https://i.imgur.com/WyrOl6y.png)

## Overview:

**Open Streaming Platform (OSP) is an open-source, RTMP streamer software front-end for [Arut's NGINX RTMP Module](https://github.com/arut/nginx-rtmp-module).**

OSP was designed a self-hosted alternative to services like Twitch.tv, Ustream.tv, and Youtube Live.

Installation Instructions may be found at the [OSP Wiki Page](https://wiki.openstreamingplatform.com)

Attribution
----
See our [Attribution Page](https://wiki.openstreamingplatform.com/en/Attribution)

Thanks
----
Special thanks to the folks of the [OSP Discord channel](https://discord.gg/Jp5rzbD) and all contributors for their code, testing, and suggestions!

License
----
MIT License